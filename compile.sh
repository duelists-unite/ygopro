#!/bin/bash
echo "Compiling..."
premake4 gmake
cd build/
make config=release
cd ..

echo "Linking..."
ln -s bin/release/ygopro ./
strip ygopro
